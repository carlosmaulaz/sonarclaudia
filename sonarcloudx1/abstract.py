from abc import ABC
from sonarqube import SonarCloudClient
import os

class AbstractSonar(ABC):

    def __init__(self, personal_access_token, organization):

        self.personal_access_token = personal_access_token
        self.sonar_url = 'https://sonarcloud.io/'
        self.organization = organization
        self.sonar = SonarCloudClient(sonarqube_url=self.sonar_url, token="95a9ce146a7cc04163cb608e8cfa207846c3398e")

    