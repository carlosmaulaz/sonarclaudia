from sonarcloudx1 import factories
import os
import json
import plantuml
import re


def retorna_string_de_um_json(name):

    with open(f'{name}.json', 'r') as arquivo:
    # Carregar o conteúdo do arquivo JSON
        dados_json = json.load(arquivo)

# Agora, a variável 'dados_json' contém o conteúdo do arquivo JSON
# Você pode acessar os dados conforme necessário
    return dados_json


def create_json(results, name):

    file = open(f'{name}.json', 'w') 
    json.dump(results, file, indent=4)
    file.close

def create_plantuml_png(plantuml_text, service):

    name = get_nome_da_entidade(service)

    with open('temp.txt', 'w') as file:
        file.write(plantuml_text)

    plantuml.PlantUML(url='http://www.plantuml.com/plantuml/img/').processes_file('temp.txt', outfile=f'{name}.png')
    os.remove('temp.txt')

def get_nome_da_entidade(service):
    string = re.split(string= str(type(service)),pattern=r"'")[1]
    string = string.split(".")  
    nome_da_entidade = string[2]  
    return nome_da_entidade 


def  create_examples(service, results):
    name = get_nome_da_entidade(service)
    os.chdir("exemplos")
    create_json(results=results, name=name)
    json_string = retorna_string_de_um_json(name)

    uml_code = "@startjson\n"
    uml_code += json.dumps(json_string)
    uml_code += "\n@endjson"

    create_plantuml_png(uml_code, service=service)

    os.chdir("..")

personal_access_token =  os.environ.get('MEU_TOKEN')
sonarcloud_organization = "tcc123"

lista1 = [factories.Issues(personal_access_token=personal_access_token, organization=sonarcloud_organization),
          factories.Project(personal_access_token=personal_access_token, organization=sonarcloud_organization),
          factories.SupportedProgramingLanguages(personal_access_token=personal_access_token, organization=sonarcloud_organization)]
lista = [
         factories.Project(personal_access_token=personal_access_token, organization=sonarcloud_organization)
        ]

for elemento in lista1:

    service = elemento
    results = service.get_all(today=False)
    
    
    #criação dos exemplos

    create_examples(service=service, results=results)







