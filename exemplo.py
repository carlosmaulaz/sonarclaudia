import json

# Abrir o arquivo JSON em modo de leitura
name="Project"

with open(f'{name}.json', 'r') as arquivo:
    # Carregar o conteúdo do arquivo JSON
    dados_json = json.load(arquivo)

# Agora, a variável 'dados_json' contém o conteúdo do arquivo JSON
# Você pode acessar os dados conforme necessário
print(dados_json)