from gitlabx import factories
from pprint import pprint 
import json
import os
import re
import plantuml

def create_plantuml_JSON_diagram(results, name):
    with open(os.path.join('exemplos', name+'.json'), 'w') as file:
        
        for item in results:
            json.dump(item, file, indent=4)

def create_PlanUML_Class_Diagram():
    os.system(f"pyreverse ./gitlabx/ -o plantuml")
    plantuml.PlantUML(url='http://www.plantuml.com/plantuml/img/').processes_file('classes.plantuml', outfile=os.path.join('exemplos','SonarClaudiaClassDiagram.png'))

    os.remove('classes.plantuml')  
    os.remove('packages.plantuml')     

def get_nome_da_entidade(service):
    string = re.split(string= str(type(service)),pattern=r"'")[1]
    string = string.split(".")  
    nome_da_entidade = string[2]  
    return nome_da_entidade 

token = os.environ.get('TOKEN_GITLAB')

personal_access_token =  token

#Lista de entidades

lista1 = [factories.Project(personal_access_token=personal_access_token), 
         factories.Commits(personal_access_token=personal_access_token),
         factories.Branches(personal_access_token=personal_access_token),
         factories.Events(personal_access_token=personal_access_token),
         factories.Deployments(personal_access_token=personal_access_token),
         factories.Issues(personal_access_token=personal_access_token),
         factories.RepositoryTree(personal_access_token=personal_access_token),
         factories.Repositories(personal_access_token=personal_access_token),
         factories.ProjectLanguages(personal_access_token=personal_access_token)]

lista = [
         factories.Project(personal_access_token=personal_access_token)
        ]

# Iterando sobre a lista usando um loop for
for elemento in lista:    

    service = elemento
    results = service.get_all(today=False)

    #Pega o nome da entidade que estamos trabalhdno
    nome_da_entidade = get_nome_da_entidade(service)
    print(str(len(results))+' '+nome_da_entidade+'\n')

    #Faz um PlantUML do Json
    uml_code = "@startjson\n"
    if not len(results)==0:
        uml_code += json.dumps(results[0], indent=4)
    uml_code += "\n@endjson"
    with open('temp.txt', 'w') as file2:
        file2.write(uml_code)

    plantuml.PlantUML(url='http://www.plantuml.com/plantuml/img/').processes_file('temp.txt', outfile=os.path.join('exemplos',nome_da_entidade+'.png'))
    os.remove('temp.txt')  


    #Cria os Exemplos
    
    create_plantuml_JSON_diagram(results, nome_da_entidade)

#Cria um diagrama de Classe dos exemplos

create_PlanUML_Class_Diagram()



